Name:                annotation-indexer
Version:             1.9
Release:             1
Summary:             Jenkins annotation-indexer library
License:             MIT
URL:                 https://github.com/jenkinsci/lib-annotation-indexer
Source0:             https://github.com/jenkinsci/lib-annotation-indexer/archive/annotation-indexer-%{version}.tar.gz
Source1:             https://raw.github.com/jenkinsci/jenkins/jenkins-1.510/LICENSE.txt
BuildArch:           noarch
BuildRequires:       maven-local mvn(org.kohsuke.metainf-services:metainf-services)
%description
Annotation-indexer is a small java library
used for listing annotations at compile time.

%package        javadoc
Summary:             Javadoc for %{name}
%description    javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n lib-%{name}-%{name}-%{version}
cp %{SOURCE1} LICENSE
%pom_remove_parent
%pom_xpath_inject "pom:project" "<groupId>org.jenkins-ci</groupId>"

%build
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%doc LICENSE

%files javadoc -f .mfiles-javadoc
%doc LICENSE

%changelog
* Thu Aug 6 2020 Anan Fu <fuanan3@huawei.com> - 1.9-1
- package init
